/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135_2018.mantenimiento.mantenimientowebapp.boundary;

import javax.ejb.EJB;
import javax.ws.rs.Path;
import sv.edu.uesocc.ingenieria.tpi135_2018.mantenimiento.mantenimientowebapp.controller.AbstractInterface;
import sv.edu.uesocc.ingenieria.tpi135_2018.mantenimiento.mantenimientowebapp.controller.EstadoFacadeLocal;
import sv.edu.uesocc.ingenieria.tpi135_2018.mantenimiento.mantenimientowebapp.entity.Estado;

/**
 *
 * @author rcarlos
 */
@Path("estado")
public class EstadoResource extends AbstractResource<Estado>{
    
    @EJB
    private EstadoFacadeLocal sfl;

    public EstadoResource() {
        super(Estado.class);
    }
    
    @Override
    protected AbstractInterface<Estado> getFacade() {
        return sfl;
    }

    @Override
    protected Estado crearNuevo() {
        return new Estado();
    }
    
}
