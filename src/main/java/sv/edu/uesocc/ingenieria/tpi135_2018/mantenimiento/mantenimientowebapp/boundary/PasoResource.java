/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135_2018.mantenimiento.mantenimientowebapp.boundary;

import javax.ejb.EJB;
import javax.ws.rs.Path;
import sv.edu.uesocc.ingenieria.tpi135_2018.mantenimiento.mantenimientowebapp.controller.AbstractInterface;
import sv.edu.uesocc.ingenieria.tpi135_2018.mantenimiento.mantenimientowebapp.controller.PasoFacadeLocal;
import sv.edu.uesocc.ingenieria.tpi135_2018.mantenimiento.mantenimientowebapp.entity.Paso;

/**
 *
 * @author rcarlos
 */
@Path("paso")
public class PasoResource extends AbstractResource<Paso> {

    @EJB
    private PasoFacadeLocal pfl;

    public PasoResource() {
        super(Paso.class);
    }

    @Override
    protected AbstractInterface<Paso> getFacade() {
        return pfl;
    }

    @Override
    protected Paso crearNuevo() {
        return new Paso();
    }

}
