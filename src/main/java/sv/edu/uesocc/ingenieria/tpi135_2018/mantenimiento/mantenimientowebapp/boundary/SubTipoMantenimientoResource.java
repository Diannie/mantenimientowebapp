/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135_2018.mantenimiento.mantenimientowebapp.boundary;

import javax.ejb.EJB;
import javax.ws.rs.Path;
import sv.edu.uesocc.ingenieria.tpi135_2018.mantenimiento.mantenimientowebapp.controller.AbstractInterface;
import sv.edu.uesocc.ingenieria.tpi135_2018.mantenimiento.mantenimientowebapp.controller.SubTipoMantenimientoFacadeLocal;
import sv.edu.uesocc.ingenieria.tpi135_2018.mantenimiento.mantenimientowebapp.entity.SubTipoMantenimiento;

/**
 *
 * @author rcarlos
 */
@Path("subtipomantenimiento")
public class SubTipoMantenimientoResource extends AbstractResource<SubTipoMantenimiento> {

    @EJB
    private SubTipoMantenimientoFacadeLocal stmfl;

    public SubTipoMantenimientoResource() {
        super(SubTipoMantenimiento.class);
    }

    @Override
    protected AbstractInterface<SubTipoMantenimiento> getFacade() {
        return stmfl;
    }

    @Override
    protected SubTipoMantenimiento crearNuevo() {
        return new SubTipoMantenimiento();
    }

}
