/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.tpi135_2018.mantenimiento.mantenimientowebapp.boundary;

import javax.ejb.EJB;
import javax.ws.rs.Path;
import sv.edu.uesocc.ingenieria.tpi135_2018.mantenimiento.mantenimientowebapp.controller.AbstractInterface;
import sv.edu.uesocc.ingenieria.tpi135_2018.mantenimiento.mantenimientowebapp.controller.OrdenFacadeLocal;
import sv.edu.uesocc.ingenieria.tpi135_2018.mantenimiento.mantenimientowebapp.entity.Orden;

/**
 *
 * @author rcarlos
 */
@Path("orden")
public class OrdenResource extends AbstractResource<Orden> {

    @EJB
    private OrdenFacadeLocal ofl;

    public OrdenResource() {
        super(Orden.class);
    }

    @Override
    protected AbstractInterface<Orden> getFacade() {
        return ofl;
    }

    @Override
    protected Orden crearNuevo() {
        return new Orden();
    }

}
